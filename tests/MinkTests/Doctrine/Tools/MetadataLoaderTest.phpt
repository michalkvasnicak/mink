<?php

namespace MinkTests\Doctrine\Tools;

use Mink\Doctrine\Factories\EntityManagerFactory;
use Mink\Doctrine\Tools\MetadataLoader;
use Nette;
use Tester;
use Tester\Assert;
use Mockery as m;


require_once __DIR__ . '/../../../bootstrap.php';
require_once __DIR__ . '/Fixtures/TestEntity.php';


/**
 * Test: MinkTests\Doctrine\Tools\MetadataLoaderTest
 *
 * @testCase MinkTests\Doctrine\Tools\MetadataLoaderTest
 * @author Michal Kvasničák <michal.kvasnicak@gmail.com>
 * @package MinkTests\Doctrine\Tools
 */
class MetadataLoaderTest extends Tester\TestCase
{


    public function testGetMetadataMethod()
    {
        $entityManager = EntityManagerFactory::create(
            false,
            [
                'entityDirs' => [__DIR__ . '/Fixtures'],
                'proxyDir' => __DIR__ . '/Fixtures',
                'entityNamespaces' => ['MinkTests\Doctrine\Tools\Fixtures'],
                'connection' => [
                    'driver' => 'pdo_sqlite',
                    'memory' => true
                ]
            ]
        );

        $metadataLoader = new MetadataLoader($entityManager);

        $metadata = $metadataLoader->getMetadata();

        Assert::true( count($metadata) == 1);
    }



    protected function tearDown()
    {
        parent::tearDown();
        m::close();
    }


}

run(new MetadataLoaderTest());

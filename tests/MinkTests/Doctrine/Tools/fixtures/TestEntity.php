<?php

namespace MinkTests\Doctrine\Tools\Fixtures;

/**
 * @author Michal Kvasnicak <michal.kvasnicak@gmail.com>
 * @copyright 2013
 *
 * @Entity
 */
class TestEntity 
{


    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    private $id;


}

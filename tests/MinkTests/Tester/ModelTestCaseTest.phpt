<?php

/**
 * Test: MinkTests\Tester\ModelTestCaseModelTestCase.
 *
 * @testCase MinkTests\Tester\ModelTestCaseTest
 * @author Michal Kvasničák <michal.kvasnicak@gmail.com>
 * @package MinkTests\Tester
 */
 
namespace MinkTests\Tester;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Mink\Tester\ModelTestCase;
use Nette\DI\Container;
use Tester;
use Tester\Assert;
use Mockery as m;


require_once __DIR__ . '/../../bootstrap.php';


/**
 * @author Michal Kvasničák <michal.kvasnicak@gmail.com>
 */
class ModelTestCaseTest extends Tester\TestCase
{


    /**
     * Tests if set up method is calling schematool createSchema
     */
    public function testSetUpMethod()
    {
        // mocks
        $metadataLoader = m::mock('Mink\Doctrine\Tools\MetadataLoader');
        $schemaTool = m::mock('Doctrine\ORM\Tools\SchemaTool');

        // what should mocks receive
        $schemaTool->shouldReceive('createSchema')->with([])->once()->andReturnNull();
        $metadataLoader->shouldReceive('getMetadata')->withNoArgs()->once()->andReturn([]);

        $testCase = new ModelTestCase($schemaTool, $metadataLoader, new Container());

        $reflection = new \ReflectionClass($testCase);
        $setUpMethod = $reflection->getMethod('setUp');
        $setUpMethod->setAccessible(true);
        $setUpMethod->invokeArgs($testCase, []);
    }


    /**
     * Tests if tear down methos is callin schematool dropSchema
     */
    public function testTearDownMethod()
    {
        // mocks
        $metadataLoader = m::mock('Mink\Doctrine\Tools\MetadataLoader');
        $schemaTool = m::mock('Doctrine\ORM\Tools\SchemaTool');

        // what should mocks receive
        $schemaTool->shouldReceive('dropSchema')->with([])->once()->andReturnNull();
        $metadataLoader->shouldReceive('getMetadata')->withNoArgs()->once()->andReturn([]);

        $testCase = new ModelTestCase($schemaTool, $metadataLoader, new Container());

        $reflection = new \ReflectionClass($testCase);
        $setUpMethod = $reflection->getMethod('tearDown');
        $setUpMethod->setAccessible(true);
        $setUpMethod->invokeArgs($testCase, []);
    }


    protected function tearDown()
    {
        parent::tearDown();

        m::close();
    }


}

run(new ModelTestCaseTest());
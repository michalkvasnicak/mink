<?php

namespace MinkTests\Tester;

use Nette;
use Tester;
use Tester\Assert;
use Nette\DI\Container;
use MinkTests\Tester\Fixtures\TestPresenter;
use Mockery as m;
use Mink\Tester\PresenterTestCase;


require_once __DIR__ . '/../../bootstrap.php';
require_once __DIR__ . '/Fixtures/TestPresenter.php';


/**
 * Test: MinkTests\Tester\PresenterTestCaseTest
 *
 * @testCase MinkTests\Tester\PresenterTestCaseTest
 * @author Michal Kvasničák <michal.kvasnicak@gmail.com>
 * @package MinkTests\Tester
 */
class PresenterTestCaseTest extends Tester\TestCase
{



    /**
     * Tests if createPresenter method is calling createPresenter on IPresenterFactory
     */
    public function testCreatePresenterMethod()
    {
        $presenter = new TestPresenter();

        //mocks
        $container = m::mock('Nette\DI\Container');
        $presenterFactoryMock = m::mock('Nette\Application\IPresenterFactory');

        //mock methods
        $container->shouldReceive('getByType')->with('Nette\Application\IPresenterFactory')->andReturn($presenterFactoryMock);
        $presenterFactoryMock->shouldReceive('createPresenter')->with('TestPresenter')->andReturn($presenter);

        $presenterTestCase = new PresenterTestCase($container);

        Assert::true($presenterTestCase->createPresenter('TestPresenter') === $presenter);
    }


    protected function tearDown()
    {
        parent::tearDown();

        m::close();
    }

}

run(new PresenterTestCaseTest());

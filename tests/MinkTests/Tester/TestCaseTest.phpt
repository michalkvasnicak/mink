<?php

namespace MinkTests\Tester;

use Nette;
use Tester;
use Tester\Assert;

$container = require_once __DIR__ . '/../../bootstrap.php';

/**
 * Test: TestCaseTest
 *
 * @author Michal Kvasničák <michal.kvasnicak@mink.sk>
 * @package MinkTests\Tester
 * @copyright Mink Ltd, 2013
 * @license https://minksro.atlassian.net/wiki/pages/viewpage.action?pageId=1572866
 */
class TestCaseTest extends Tester\TestCase
{


    public function testConstructorAndGetter()
    {
        $container = new Nette\DI\Container();
        $testCase = new \Mink\Tester\TestCase($container);

        Assert::same($testCase->getContainer(), $container);
    }

}

run(new TestCaseTest($container));

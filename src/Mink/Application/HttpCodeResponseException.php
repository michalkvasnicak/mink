<?php

namespace Mink\Application;

use Nette\Http\IResponse;
use Nette;

/**
 * Class HttpCodeResponseException
 *
 * @author Michal Kvasničák <michal.kvasnicak@mink.sk>
 * @package Mink\Application\Responses
 * @copyright Mink Ltd, 2013
 * @license https://minksro.atlassian.net/wiki/pages/viewpage.action?pageId=1572866
 */
class HttpCodeResponseException extends \Exception
{

    /**
     * @param null $message
     * @param int $code http code to send
     * @param \Exception $previous
     *
     */
    public function __construct($message = null, $code = IResponse::S204_NO_CONTENT, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

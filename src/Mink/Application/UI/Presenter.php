<?php

namespace Mink\Application\UI;

use Nette\Http\IResponse;
use Mink\Application\HttpCodeResponseException;


/**
 * Class Presenter
 *
 * @author Michal Kvasničák <michal.kvasnicak@mink.sk>
 * @package Mink\Application\UI
 * @copyright Mink Ltd, 2013
 * @license https://minksro.atlassian.net/wiki/pages/viewpage.action?pageId=1572866
 */
class Presenter extends \Nette\Application\UI\Presenter
{

    protected function sendResponseCode($message = null, $code = IResponse::S204_NO_CONTENT)
    {
        throw new HttpCodeResponseException($message, $code);
    }
}

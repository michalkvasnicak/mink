<?php

namespace Mink\Doctrine\Factories;

use Nette\Object;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\EventManager;

/**
 * @author Michal Kvasnicak <michal.kvasnicak@gmail.com>
 * @copyright 2013
 */
class EntityManagerFactory extends Object
{


    /**
     * Creates instance of entity manager
     *
     * @param bool $debugMode
     * @param array $settings
     * @param \Doctrine\Common\EventManager $ev
     * @internal param array $connection
     * @return \Doctrine\ORM\EntityManager
     */
    public static function create($debugMode, array $settings, EventManager $ev = null, $useSimpleAnnotationDriver = true)
    {
        // if debug mode is true, will check for cache
        $setup = Setup::createAnnotationMetadataConfiguration(
            (array) $settings['entityDirs'],
            $debugMode,
            $settings['proxyDir'],
            null,
            $useSimpleAnnotationDriver
        );

        $setup->setNamingStrategy(new UnderscoreNamingStrategy());
        $setup->setProxyDir($settings['proxyDir']);

        if (is_array($settings['entityNamespaces']))
        {
            $setup->setEntityNamespaces($settings['entityNamespaces']);
        }
        else
        {
            $setup->setEntityNamespaces([$settings['entityNamespaces']]);
        }

        return EntityManager::create($settings['connection'], $setup, $ev);
    }

}

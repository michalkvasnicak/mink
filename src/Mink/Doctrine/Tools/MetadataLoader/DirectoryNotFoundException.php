<?php

namespace Mink\Doctrine\Tools\MetadataLoader;

/**
 * @author Michal Kvasnicak <michal.kvasnicak@gmail.com>
 * @copyright 2013
 */
class DirectoryNotFoundException extends \RuntimeException
{

}

<?php

namespace Mink\Doctrine\Tools;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Mink\Doctrine\Tools\MetadataLoader\DirectoryNotFoundException;
use Nette\Object;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * @author Michal Kvasnicak <michal.kvasnicak@gmail.com>
 * @copyright 2013
 */
class MetadataLoader extends Object
{

    /** @var \Doctrine\ORM\EntityManager  */
    private $entityManager;


    /**
     * Creates instance of MetadataLoader
     *
     * @param \Doctrine\ORM\EntityManager $manager
     *
     * @throws MetadataLoader\DirectoryNotFoundException
     */
    public function __construct(EntityManager $manager)
    {
        $this->entityManager = $manager;
    }


    /**
     * Instance of EntityManager
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }


    /**
     * Gets metadata of all entities in directory and its subdirectories
     * @return array
     */
    public function getMetadata()
    {
        return $this->entityManager->getMetadataFactory()->getAllMetadata();
    }


}

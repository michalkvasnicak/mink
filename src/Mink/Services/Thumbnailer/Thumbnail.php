<?php

namespace Mink\Services\Thumbnailer;

use Nette\Object;

/**
 * @author Michal Kvasnicak <michal.kvasnicak@gmail.com>
 * @copyright 2013
 */ 
class Thumbnail extends Object
{


    /** @var int */
    private $width;


    /** @var int */
    private $height;


    /** @var string */
    private $name;


    /**
     * @param string $name
     * @param int $width
     * @param int $height
     */
    public function __construct($name, $width, $height)
    {
        $this->name = (string) $name;
        $this->width = (int) $width;
        $this->height = (int) $height;
    }


    /**
     * Gets thumbnail prefix name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Sets thumbnail prefix name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string) $name;
    }


    /**
     * Gets thumbnail width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }


    /**
     * Sets thumbnail width
     *
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = (int) $width;
    }


    /**
     * Gets thumbnail height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }


    /**
     * Sets thumbnail height
     *
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = (int) $height;
    }

}

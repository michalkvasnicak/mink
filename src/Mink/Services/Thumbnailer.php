<?php

namespace Mink\Services;

use Mink\Services\Thumbnailer\Thumbnail;
use Nette\Http\FileUpload;
use Nette\Object;

/**
 * @author Michal Kvasnicak <michal.kvasnicak@gmail.com>
 * @copyright 2013
 */ 
class Thumbnailer extends Object
{


    /**
     * @var string
     */
    private $destination;


    /**
     * Queue of files to process
     *
     * @var array|FileUpload[]
     */
    private $queue = [];


    /**
     * Queue of sizes to generate
     *
     * @var array|Thumbnail[]
     */
    private $thumbnails = [];



    /**
     * @param string|null $destination
     * @throws \InvalidArgumentException
     */
    public function __construct($destination = null)
    {
        if ($destination !== null)
        {
            $this->setDestination($destination);
        }
    }


    /**
     * Sets destination path to store images and thumbnails to
     *
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->initializeDestinationDir($destination);
        $this->destination = $destination;
    }


    /**
     * Gets destination path
     *
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }


    /**
     * Adds thumbnail size
     *
     * @param string $name
     * @param int $width
     * @param int $height
     *
     * @return $this
     */
    public function addThumbnail($name, $width, $height)
    {
        $this->thumbnails[] = new Thumbnail($name, $width, $height);

        return $this;
    }


    /**
     * Removes registered thumbnails sizes
     */
    public function removeThumbnails()
    {
        $this->thumbnails = [];
    }


    /**
     * Adds file to queue
     *
     * @param \Nette\Http\FileUpload $file
     * @param string $filename
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function addFile(FileUpload $file, $filename)
    {
        if (isset($this->queue[$filename]))
        {
            throw new \InvalidArgumentException("File '$filename' is already in queue.");
        }

        $this->queue[$filename] = $file;

        return $this;
    }


    /**
     * Processes all files and generates thumbnails and originals for them
     *
     * @throws \LogicException
     */
    public function process()
    {
        if (empty($this->thumbnails))
        {
            throw new \LogicException('You did not provide thumbnails to thumbnailer. What it has to generate?!');
        }

        if (empty($this->queue))
        {
            throw new \LogicException('There are no files in queue to process!');
        }

        //iterater through files and generate thumbnails of different sizes from thumbnails
        foreach ($this->queue as $filename => $file)
        {
            // save original
            $file->move($this->destination . DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $filename . '.jpg');

            foreach ($this->thumbnails as $thumb)
            {
                // call generateThumbnail
                $this->generateThumbnail(clone $file, $filename, $thumb);
            }
        }

        //unset queue
        $this->queue = [];
    }


    /**
     * Initializes dir
     *
     * @param string $destination
     * @throws \InvalidArgumentException
     */
    private function initializeDestinationDir($destination)
    {
        if (! is_dir($destination))
        {
            throw new \InvalidArgumentException("'$destination' is not a valid folder.");
        }

        if (! is_writable($destination))
        {
            throw new \InvalidArgumentException("'$destination' is not writable.");
        }

        // create thumbnails and original dir if dont exists
        $dest = rtrim($destination, DIRECTORY_SEPARATOR);
        $original = $dest . DIRECTORY_SEPARATOR . 'original';
        $thumbnail = $dest . DIRECTORY_SEPARATOR . 'thumbnail';

        if (! is_dir($original))
        {
            mkdir($original);
        }

        if (! is_dir($thumbnail))
        {
            mkdir($thumbnail);
        }
    }


    /**
     * Generates thumbnail for file
     *
     * @param \Nette\Http\FileUpload $file
     * @param string $filename
     * @param Thumbnailer\Thumbnail $thumbnail
     */
    protected function generateThumbnail(FileUpload $file, $filename, Thumbnail $thumbnail)
    {
        $image = $file->toImage();

        // zistit ci zmensujeme alebo zvacsujeme obrazok
        // zistit ci obrazok menime k vyske alebo sirke
        // potom orezat

        $x = ($image->getWidth() <= $image->getHeight()) ? $thumbnail->getWidth() : null;
        $y = ($image->getWidth() > $image->getHeight()) ? $thumbnail->getHeight() : null;

        $filename = sprintf(
            '%s/%s/%s_%s.jpg',
            $this->destination,
            'thumbnail',
            $thumbnail->getName(),
            $filename
        );

        $resizedImage = $image->resize($x, $y);

        //crop image to center
        $width = $resizedImage->getWidth() - $thumbnail->getWidth();
        $height = $resizedImage->getHeight() - $thumbnail->getHeight();

        $calculatedCropSize = [
            'x' => round($width / 2),
            'y' => 0,
            'w' => $thumbnail->getWidth(),
            'h' => $thumbnail->getHeight()
        ];

        $resizedImage->crop(
            $calculatedCropSize['x'],
            $calculatedCropSize['y'],
            $calculatedCropSize['w'],
            $calculatedCropSize['h']
        );

        // save image as 80% jpeg
        $resizedImage->save($filename, 80, $image::JPEG);
    }

}

<?php

namespace Mink\Tester;

use Nette\Application\IPresenterFactory;
use Nette\DI\Container;

/**
 * @author Michal Kvasnicak <michal.kvasnicak@gmail.com>
 * @copyright 2013
 */
class PresenterTestCase extends TestCase
{


    /**
     * Creates presenter by its Nette name
     *
     * @param string $name
     * @return \Nette\Application\IPresenter
     */
    public function createPresenter($name)
    {
        /** @var IPresenterFactory $presenterFactory */
        $presenterFactory = $this->getContainer()->getByType('Nette\Application\IPresenterFactory');

        $presenter = $presenterFactory->createPresenter($name);

        $presenter->autoCanonicalize = false; // turn off redirects

        return $presenter;
    }

}

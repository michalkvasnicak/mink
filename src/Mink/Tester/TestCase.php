<?php

namespace Mink\Tester;

use Nette\DI\Container;
use Tester;

/**
 * Class TestCase
 *
 * @author Michal Kvasničák <michal.kvasnicak@mink.sk>
 * @package Mink\Tester
 * @copyright Mink Ltd, 2013
 * @license https://minksro.atlassian.net/wiki/pages/viewpage.action?pageId=1572866
 */
class TestCase extends Tester\TestCase
{

    /** @var \Nette\DI\Container|\SystemContainer|null */
    private $container;


    /**
     * @param Container|null $container DI container
     */
    public function __construct(Container $container = null)
    {
        $this->container = $container;
    }


    /**
     * Gets DI container
     *
     * @return Container|\SystemContainer|null
     */
    public function getContainer()
    {
        return $this->container;
    }



}

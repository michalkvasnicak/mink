<?php

namespace Mink\Tester;

use Doctrine\ORM\Tools\SchemaTool;
use Mink\Doctrine\Tools\MetadataLoader;
use Nette\DI\Container;

/**
 * @author Michal Kvasnicak <michal.kvasnicak@gmail.com>
 * @copyright 2013
 */
class ModelTestCase extends TestCase
{


    /** @var \Doctrine\ORM\Tools\SchemaTool  */
    private $schemaTool;


    /** @var \Mink\Doctrine\Tools\MetadataLoader  */
    private $metadataLoader;


    public function __construct(SchemaTool $schemaTool, MetadataLoader $loader, Container $container)
    {
        $this->schemaTool = $schemaTool;
        $this->metadataLoader = $loader;
        parent::__construct($container);
    }


    /**
     * Schema tool instance
     *
     * @return SchemaTool
     */
    public function getSchemaTool()
    {
        return $this->schemaTool;
    }


    /**
     * Instance of Class Metadata Loader
     *
     * @return MetadataLoader
     */
    public function getMetadataLoader()
    {
        return $this->metadataLoader;
    }


    /**
     * Creates schema
     */
    protected function setUp()
    {
        parent::setUp();

        $this->schemaTool->createSchema($this->metadataLoader->getMetadata());
    }


    /**
     * Drops schema
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->schemaTool->dropSchema($this->metadataLoader->getMetadata());
    }


}
